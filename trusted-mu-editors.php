<?php
/*
Plugin Name: Trusted MU Editors
Plugin URI: https://bitbucket.org/mapdigital/wpplugin-trusted-mu-editors/
Description: Allows blog administrators to configure which user roles are allowed to post unfiltered HTML in a multisite environment. By default this privilege is granted to Administrators only.
Author: Map Digital
Version: 1
Author URI: http://mapdigital.com.au/
*/

abstract class TrustedMUEditors
{
	public static function init()
	{
		$cls = get_class();

		// core plugin functionality
		add_action('init', array($cls, 'checkDisableKses'), 11);
		add_action('set_current_user', array($cls, 'checkDisableKses'), 11);

		// add configuration UI
		add_action('admin_menu', array($cls, 'setupAdminScreens'));
		add_action('load-settings_page_trusted-mu-editors', array($cls, 'handleOptions'));
		add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($cls, 'addPluginPageLinks'));

		// uninstall & installation hooks
		register_activation_hook(__FILE__, array($cls, 'runInstall'));
		register_deactivation_hook(__FILE__, array($cls, 'runUninstall'));
	}

	//--------------------------------------------------------------------------

	// remove post Kses if user has unfiltered posting capability
	public static function checkDisableKses()
	{
		global $current_user;

		if (self::checkUserCap($current_user)) {
			kses_remove_filters();
		}
	}

	// adds the unfiltered posting capability to all roles configured to receive it
	private static function addCapability()
	{
		$roles = self::getRoles();
		$allowed = get_option('trusted_mu_editors');

		foreach (array_keys($allowed) as $roleName) {
			if (!isset($roles[$roleName])) {
				continue;
			}
			$roles[$roleName]->add_cap('unfiltered_html');
		}
	}

	// removes the unfiltered posting capability from all roles
	private static function removeCapability()
	{
		$roles = self::getRoles();

		foreach ($roles as $role) {
			$role->remove_cap('unfiltered_html');
		}
	}

	// synchronises user-level capabilities depending on the roles assigned to them
	private static function refreshUserCapabilities()
	{
		$users = get_users();
		$roles = self::getRoles();

		foreach ($users as $user) {
			$shouldHaveCap = false;
			foreach ($user->roles as $roleName) {
				if (isset($roles[$roleName]) && $roles[$roleName]->has_cap('unfiltered_html')) {
					$shouldHaveCap = true;
					break;
				}
			}

			if ($shouldHaveCap && !self::checkUserCap($user)) {
				$user->add_cap('unfiltered_html');
			} else if (self::checkUserCap($user)) {
				$user->remove_cap('unfiltered_html');
			}
		}
	}

	// get the Wordpress role objects array
	public static function &getRoles()
	{
		global $wp_roles;
		get_role('administrator');	// forces $wp_roles to load

		return $wp_roles->role_objects;
	}

	// check the user capability our own way, by bypassing has_cap() since map_meta_cap() will mask the capability out in Multisite
	public static function checkUserCap($user)
	{
		return !empty($user->caps['unfiltered_html']);
	}

	//--------------------------------------------------------------------------

	public static function runInstall()
	{
		update_option('trusted_mu_editors', array('administrator' => 1));
		self::addCapability();
		self::refreshUserCapabilities();
	}

	public static function runUninstall()
	{
		self::removeCapability();
		self::refreshUserCapabilities();
		delete_option('trusted_mu_editors');
	}

	//--------------------------------------------------------------------------

	public static function setupAdminScreens()
	{
		add_submenu_page('options-general.php',  __('Trusted Editors'), __('Trusted Editors'), 'manage_options', 'trusted-mu-editors', array(get_class(), 'drawSettingsPage'));
	}

	public static function drawSettingsPage()
	{
		include('settings-page.php');
	}

	public static function handleOptions()
	{
		if (!empty($_POST)) {
			update_option('trusted_mu_editors', $_POST['trusted_roles']);	// set which roles to apply the capability to
			self::removeCapability();			// clear previous assignments
			self::addCapability();				// re-add capability to roles needing it
			self::refreshUserCapabilities();	// sync user-level data with role caps

			add_action('admin_notices', array(get_class(), 'handleUpdateNotice'));
		}
	}

	public static function handleUpdateNotice()
	{
		echo '<div class="updated"><p>Permissions saved.</p></div>';
	}

	public static function addPluginPageLinks($links)
	{
		array_unshift($links, '<a href="options-general.php?page=trusted-mu-editors">Settings</a>');
		return $links;
	}
}

TrustedMUEditors::init();
