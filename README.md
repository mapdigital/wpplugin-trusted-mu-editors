Trusted MU Editors
==================

This plugin allows blog administrators in a Multisite environment to specify user roles which are allowed to create posts with unfiltered HTML. These users will be able to make posts containing `<iframe>`, `<embed>` etc without that content being stripped by Multisite's sanitisation rules.

The plugin can either be used globally for your entire network, or it can be applied on a blog-by-blog basis. Regular WordPress already allows this feature and does not need this plugin.

Installation
------------

Preferred means of including this plugin in your project is via git submodules. Assuming you are versioning from the root of your Wordpress site: `git submodule add git@bitbucket.org:mapdigital/wpplugin-trusted-mu-editors.git wp-content/plugins/trusted-mu-editors`

Activate this plugin for those blogs on which you want this feature enabled or enable it sitewide with the "network activate" feature in the Network Admin dashboard. Deactivating the plugin will remove the capablitiy for the blog(s) it was removed from.

*:NOTE:* this plugin should be installed into `wp-content/plugins`, not into `wp-content/mu-plugins`.

Troubleshooting
---------------

If for any reason the `unfiltered_html` capability is ever lost to users who should genuinely have it, simply deactivate & reactivate this plugin.
