<?php
/**
 * Trusted MU Editors admin settings page HTML
 *
 * @author	Sam Pospischil <pospi@spadgos.com>
 * @since	23 Jul 2013
 */

$roles = TrustedMUEditors::getRoles();

?>
<h1>Trusted Multisite Editors</h1>

<p>Allows configuration of user roles allowed to post unfiltered HTML in post content.</p>
<p><strong style="color: #F00;">WARNING:</strong> this is a huge security risk. Never allow untrusted users to have this functionality!</p>

<form method="post" id="tme-settings">
	<h3>Allowed roles</h3>
	<ul>
	<?php foreach ($roles as $name => $role) : ?>
		<li><label><input type="checkbox" name="trusted_roles[<?php echo $name; ?>]" id="jsmts_do_flag"<?php if ($role->has_cap('unfiltered_html')) echo ' checked="checked"'; ?> /> <?php echo ucfirst($role->name); ?></label></li>
	<?php endforeach; ?>
	</ul>
	<p>
		<input type="submit" name="save" value="Update privileges" />
	</p>
</form>
